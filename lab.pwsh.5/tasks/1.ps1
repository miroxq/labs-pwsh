
function Replace-Words()
{
    [CmdletBinding()]
    param(
        [Parameter(ValueFromPipeline, Mandatory)]
        [string]$data,
        [Parameter(ValueFromPipeline, Mandatory)]
        [string]$words
    )
    $replacing_words = $words.Split("=")
    $data=$data.Replace($replacing_words[0],$replacing_words[1])
    return $data
}

$replaceword = Read-Host "Enter replaceble word: "
$replace = Read-Host "Enter replacement for words: "
Set-Location C:\Users\miroxq\Desktop\pwsh
Get-Content .\slova.txt | Replace-Words -words "$replaceword=$replace" | Set-Content .\slova.txt