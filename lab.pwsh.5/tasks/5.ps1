﻿
function Save-ImageMetaData {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true)]
        [string]$Path,
        [string[]]$Extension = "jpg",
        [ValidateSet("csv","xlsx","html")]
        [string]$OutFormat = "csv"
    )

    $files = Get-ChildItem -Path $Path -Filter "*.$Extension" -Recurse
    $metadata = @()
    foreach ($file in $files) {
        $properties = @{
            "Name" = $file.Name
            "Extension" = $file.Extension
            "Size" = $file.Length
            "Height" = 0
            "Width" = 0
            "Created" = $file.CreationTime
        }

        if ($file.Extension -match "jpg|jpeg|png|bmp|gif") {
            $image = [System.Drawing.Image]::FromFile($file.FullName)
            $properties["Height"] = $image.Height
            $properties["Width"] = $image.Width
            $image.Dispose()
        }

        $metadata += New-Object PSObject -Property $properties
    }

    switch ($OutFormat) {
        "csv" { $metadata | Export-Csv -Path "$Path\Informacion.csv" -NoTypeInformation -Force }
        "xlsx" { $metadata | Export-Excel -Path "$Path\Informacion.xlsx" -AutoSize -FreezeTopRow -WorksheetName "MetaData" -Force }
        "html" { $metadata | ConvertTo-Html -Property Name,Extension,Size,Height,Width,Created | Out-File "$Path\Informacion.html" -Encoding UTF8 -Force }
    }

    $acl = Get-Acl "$Path\Informacion.$OutFormat"
    #$rule = New-Object System.Security.AccessControl.FileSystemAccessRule("Users","ReadAndExecute","Allow")
    #$acl.SetAccessRule($rule)
    Set-Acl "$Path\Informacion.$OutFormat" $acl
}

Save-ImageMetaData -Path C:\Users\miroxq\Desktop\pwsh -Extension jpg -OutFormat csv