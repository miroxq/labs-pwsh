
function Remove-OldFiles()
{
    [CmdletBinding()]
    param(
        [Parameter(ValueFromPipeline, Mandatory)]
        [string]$path = "C:\Users\miroxq\Desktop\pwsh",
        [Parameter(ValueFromPipeline)]
        [DateTime]$daycount = (Get-Date).AddDays(-45) 
    )
    try{
    $for_delete = Get-ChildItem -Path $path
    $for_delete | Where-Object -Property LastWriteTime -lT $daycount | Remove-Item -ErrorAction Stop
    }
    catch{
    Write-Host "File is using"
    }
}

$path = "C:\Users\miroxq\Desktop\pwsh"
#$daysold = Read-Host "Enter the days: "
$daycounter = (Get-Date).AddDays(-$daysold)
Remove-OldFiles -path "$path" -daycount $daycounter