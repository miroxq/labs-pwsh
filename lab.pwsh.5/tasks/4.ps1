﻿<#
Desription: check-links
#>
function Check-Links {
    param(
        [switch]$Verbose,
        [switch]$Debug
    )

    $shell = New-Object -ComObject WScript.Shell
    $desktopPath = $shell.SpecialFolders.Item("Desktop")
    $badLinksPath = "$desktopPath\BadLinks"
    # Need dir with name BadLinks
    if (!(Test-Path $badLinksPath)) {
        New-Item -ItemType Directory -Path $badLinksPath | Out-Null
    }

    Get-ChildItem "$desktopPath\*.lnk" | ForEach-Object {
        $linkName = $_.Name
        $linkTarget = (New-Object -ComObject WScript.Shell).CreateShortcut($_.FullName).TargetPath

        if (Test-Path $linkTarget) {
            if ($Verbose) {
                Write-Host "Link '$linkName' points to '$linkTarget'."
            }
        } 
        else {
            if ($Verbose) {
                Write-Host "Link '$linkName' points to '$linkTarget', which does not exist."
            }
            Move-Item $_.FullName $badLinksPath
            if ($Verbose) {
                Write-Host "Moved link '$linkName' to '$badLinksPath'."
            }
        }
    }
}
# Example
Check-Links -Verbose -Debug