﻿
function Create-Links {
    param(
        [Parameter(Mandatory=$true)]
        [string]$Path
    )
    $shell = New-Object -ComObject WScript.Shell
    $desktopPath = $shell.SpecialFolders.Item("Desktop")
    Get-ChildItem $Path -Recurse -Include "*.exe" | ForEach-Object {
        $shortcut = $shell.CreateShortcut("$desktopPath\$($_.Name).lnk")
        $shortcut.TargetPath = $_.FullName
        $shortcut.Save()
    }
}
# Example:
Create-Links "C:\Users\miroxq\Desktop\pwsh"